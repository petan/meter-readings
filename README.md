
# Meter Readings REST API

This is a simple RESTful web service for storing and validating meter consumption data. This web service is made using Spring MVC.

Application is using in-memory H2 database so it can be used out of the box without additional setup. When application is running database content can be viewed at http://localhost:8080/h2 with following connection data:

    Driver Class: org.h2.Driver
    JDBC URL: jdbc:h2:mem:meter_readings
    User Name: root
    Password:

Because of application requirements I couln't follow some general rules for REST API responses and status codes. Because all data is  arriving in batches to REST API and is validated in batches, there was no logic for me to implement CRUD operations over single ratio or measurement resource. For example, validation requirement for profiles states that their sum must be 1.0 (100%). If modification of single profile was allowed, validation would be broken for sure. This conclusion led me to implement every CRUD operation to work with multiple data instead with single resource.

It is possible to delete and update profiles and meter readings only for complete year. Also, it is possible to provide optional parameter (profile name or connection id) to remove only specific elements for given year.
When creating or updating data in batches - response contains list of successfull items, list of failed items, and status object with status message and list of errors if they exist. I decided to go with this because there are validation requirements that dictate to reject invalid data, but also store correct data. For such cases, response has status code 200 (OK), but from failed list and list of erros it is posible to determine what is saved and what is not.
Delete operations are returning status code 200 (OK) for success, together with number of successfull deletes. If nothing deleted, status code 404 (Not found) is returned.
For GET operations there is one endpoint with optional filters (year and profile name or connectionId).

Notes:

 - When specifing meter measurement values in json file, they should start from zero for this task. In real application there would be implementation to search for last measurement of previous year and continue to calculate consumption from there. Here this is not the case, application expects for measurement to start from zero to correctly validate the data.
 - It is possible to send additional parameter {year} in profile and reading elements in json. If {year} parameter is left out, application will assume current year for further calculations and storage.
 - I'm aware of some remaining tight coupling in controllers when saving and updating profiles and readings. This happened because I wanted to return detailed information about what exactly is stored, what is not and which errors emerged. Because of this I had to pass two lists (success and fail) and errors object between validation methods. Currently it is done by passing all of this  information as method parameters and they get updated inside. This makes it difficult to unit test some of controller methods. If I had more time I would try to improve this situation by creating additional object that contains all three parameters which would then be used as return value.

## Running tests for the Meter Readings REST API
If Maven is not installed on the system, it is possible to use mvnw instead.

    mvn test

## Starting the Meter Readings REST API
To start this web service, install [Maven](https://maven.apache.org/install.html) and execute the following command

    mvn spring-boot:run

Once the web service is started, it can be reached at

    http://localhost:8080/api/profiles

## REST Endpoints
The following REST endpoints are available upon deployment of the order management system:

Profile - ratio:

| HTTP Verb        | URL           | Description  | Status Codes |
| ----- |-----------|:-------------| ------|
| `GET` | `http://localhost:8080/api/profiles` | Obtains a list of all existing profiles | `200 OK` Returns  empty list if no profiles are found |
| `GET` | `http://localhost:8080/api/profiles?name={name}&year={year}` | Obtains profiles corresponding to the supplied {year} and {name} parameters | `200 OK` Returns  empty list if no profiles are found |
| `POST` | `http://localhost:8080/api/profiles` | Creates new profiles for current year (default) based on the JSON payload contained in the request body. It is possible to create profiles for different year if payload data supplies it's own year in {year} property  | `200 OK` If order successfully created |
| `PUT` | `http://localhost:8080/api/profiles/year/{year}` | Updates profile data for specified year with the JSON data contained in the request body | `200 OK` if order successfully updated `404 Not Found` if order does not exist |
| `DELETE` | `http://localhost:8080/api/profiles?year={year}&name={name}` | Deletes existing profiles based on mandatory {year} and optional {name} parameter | `200 OK` Response body contains number of successful deleted profiles `404 Not Found` if profiles do not exist |


Readings:

| HTTP Verb        | URL           | Description  | Status Codes |
| ----- |-----------|:-------------| ------|
| `GET` | `http://localhost:8080/api/readings` | Obtains a list of all existing readings | `200 OK` Returns  empty list if no readings are found |
| `GET` | `localhost:8080/api/readings?year={yyyy}&connectionId={id}` | Obtains readings corresponding to the supplied {year} and {id} parameters | `200 OK` Returns  empty list if no readings are found |
| `POST` | `http://localhost:8080/api/readings` | Creates new readings for current year (default) based on the JSON payload contained in the request body. It is possible to create readings for different year if payload data supplies it's own year in {year} property  | `200 OK` If order successfully created |
| `DELETE` | `http://localhost:8080/api/readings?year={yyyy}&connectionId={id}` | Deletes existing readings based on mandatory {year} and optional {id} parameter | `200 OK` Response body contains number of successfully deleted readings `404 Not Found` if readings do not exist |
| `GET` | `localhost:8080/api/readings/consumption/connection/{id}/month/{yyyyMM}` | Obtains consumption for single connection and month specified in parameters | `200 OK` Consumption result for found connection and month `404 Not Found` If consumption reading does not exist |