package com.jobinterview.meterreadings.service;

import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ProfileSaveUpdateDto;

public interface ProfileService {

    List<ProfileDto> getProfiles(Map<String, String> searchParams);

    Profile getProfileByProfileIdentity(ProfileIdentity profileIdentity);

    List<Profile> saveAllProfiles(List<Profile> profiles);

    int deleteProfilesByNameAndYear(String name, Integer year);

    List<Profile> getProfilesByNameListAndYear(List<String> nameList, Integer year);

    List<Profile> getSortedListOfRelevantProfiles(List<Reading> readings, Integer year);

    ProfileSaveUpdateDto saveAllProfileDtos(List<ProfileDto> profiles);

}
