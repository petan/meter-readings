package com.jobinterview.meterreadings.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.dto.ReadingSaveUpdateDto;
import com.jobinterview.meterreadings.mapper.ReadingMapper;
import com.jobinterview.meterreadings.repository.ReadingRepository;
import com.jobinterview.meterreadings.util.DateUtil;
import com.jobinterview.meterreadings.validator.ReadingDtoValidator;
import com.jobinterview.meterreadings.validator.ReadingValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ReadingServiceImpl implements ReadingService {

    private final ReadingRepository readingRepository;
    private final ReadingMapper readingMapper;
    private final ReadingValidator readingValidator;
    private final ReadingDtoValidator readingDtoValidator;

    public ReadingServiceImpl(ReadingRepository readingRepository, ReadingMapper readingMapper, ReadingValidator readingValidator,
                              ReadingDtoValidator readingDtoValidator) {
        this.readingRepository = readingRepository;
        this.readingMapper = readingMapper;
        this.readingValidator = readingValidator;
        this.readingDtoValidator = readingDtoValidator;
    }

    @Override
    public List<ReadingDto> getReadings(Map<String, String> searchParams) {

        return readingMapper.mapToDto(readingRepository.findReadings(searchParams));
    }

    @Override
    public Reading getReadingByReadingIdentity(ReadingIdentity readingIdentity) {

        return readingRepository.findByReadingIdentity(readingIdentity);
    }

    @Override
    public int deleteReadingsByConnectionAndYear(Long connectionId, Integer year) {
        return readingRepository.deleteReadingsByConnectionAndYear(connectionId, year);
    }

    @Override
    public List<Reading> saveAllReadings(List<Reading> readings, List<Reading> failed, List<String> errors) {
        List<Reading> success = new ArrayList<>();
        readings.forEach(reading -> {
            Reading temp = saveReadingIfNotExists(reading);
            if(temp != null) {
                success.add(temp);
            }
            else {
                errors.add("ConnectionId: "
                        + reading.getReadingIdentity().getConnectionID() + " "
                        + DateUtil.monthFromDate(reading.getReadingIdentity().getDate(), "MMM")
                        + " rejected - already exists");
                failed.add(reading);
            }
        });
        return success;
    }

    @Override
    public Long getConsumptionForConnectionAndMonth(Long connectionId, Date date) {
        Date previousMonth = DateUtil.addMonths(date, -1);
        Reading thisMonth = readingRepository.getReadingForConnectionAndMonth(connectionId, date);
        Reading previous = readingRepository.getReadingForConnectionAndMonth(connectionId, previousMonth);

        if(thisMonth == null) {
            return null; // return 404 not found
        } else if(previous != null) {
            return thisMonth.getReading() - previous.getReading();
        } else {
            //no previous reading - assume previous reading = 0 (new connection)
            return thisMonth.getReading();
        }
    }

    @Override
    public ReadingSaveUpdateDto saveAllReadingDtos(List<ReadingDto> readingDtoList) {
        ReadingSaveUpdateDto dto = new ReadingSaveUpdateDto();
        readingDtoValidator.validate(readingDtoList, dto.getFail(), dto.getStatus().getErrors());
        List<Reading> readings = readingMapper.mapFromDto(readingDtoList);

        List<Reading> failed = new ArrayList<>();
        readingValidator.validate(readings, failed, dto.getStatus().getErrors());
        dto.getFail().addAll(readingMapper.mapToDto(failed));

        dto.getSuccess().addAll(readingMapper.mapToDto(saveAllReadings(readings, failed, dto.getStatus().getErrors())));
        dto.getFail().addAll(readingMapper.mapToDto(failed));

        if(dto.getStatus().getErrors().size() > 0) {
            dto.getStatus().setErrorMessage("Validation failed. " + dto.getStatus().getErrors().size() + " error(s)");
        }
        return dto;
    }

    private Reading saveReadingIfNotExists(Reading reading) {

        // the task is that duplicated entries should be rejected
        if(!checkReadingExists(reading)) {
            return readingRepository.save(reading);
        } else {
            return null;
        }
    }

    private boolean checkReadingExists(Reading reading) {

        Reading checkReading = getReadingByReadingIdentity(reading.getReadingIdentity());
        return checkReading != null;
    }
}
