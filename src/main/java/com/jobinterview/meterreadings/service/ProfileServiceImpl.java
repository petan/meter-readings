package com.jobinterview.meterreadings.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ProfileSaveUpdateDto;
import com.jobinterview.meterreadings.mapper.ProfileMapper;
import com.jobinterview.meterreadings.repository.ProfileRepository;
import com.jobinterview.meterreadings.validator.ProfileDtoValidator;
import com.jobinterview.meterreadings.validator.ProfileValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Slf4j
@Service
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;
    private final ProfileDtoValidator profileDtoValidator;
    private final ProfileValidator profileValidator;
    private final ProfileMapper profileMapper;

    public ProfileServiceImpl(ProfileRepository profileRepository, ProfileDtoValidator profileDtoValidator,
                              ProfileValidator profileValidator, ProfileMapper profileMapper) {
        this.profileRepository = profileRepository;
        this.profileDtoValidator = profileDtoValidator;
        this.profileValidator = profileValidator;
        this.profileMapper = profileMapper;
    }

    @Override
    public List<ProfileDto> getProfiles(Map<String, String> searchParams) {
        return profileMapper.mapToDto(profileRepository.findProfiles(searchParams));
    }

    public Profile getProfileByProfileIdentity(ProfileIdentity profileIdentity) {
        return profileRepository.findByProfileIdentity(profileIdentity);
    }

    @Override
    public int deleteProfilesByNameAndYear(String name, Integer year) {
        return profileRepository.deleteProfilesByNameAndYear(name, year);
    }

    @Override
    public List<Profile> getProfilesByNameListAndYear(List<String> nameList, Integer year) {
        if(nameList != null && nameList.size() > 0)
            return profileRepository.findProfilesByNameListAndYear(nameList, year);
        else
            return new ArrayList<>();
    }

    @Override
    @Transactional
    public List<Profile> saveAllProfiles(List<Profile> profiles) {

        return profiles.stream().map(profile -> saveOrUpdateProfile(profile)).collect(Collectors.toList());
    }

    @Override
    public List<Profile> getSortedListOfRelevantProfiles(List<Reading> readings, Integer year) {
        List<String> distinctNames = readings.stream().map(Reading::getProfileName).distinct().collect(Collectors.toList());
        return getProfilesByNameListAndYear(distinctNames, year);
    }

    @Override
    public ProfileSaveUpdateDto saveAllProfileDtos(List<ProfileDto> profileDtoList) {
        ProfileSaveUpdateDto dto = new ProfileSaveUpdateDto();
        profileDtoValidator.validate(profileDtoList, dto.getFail(), dto.getStatus().getErrors());
        List<Profile> profiles = profileMapper.mapFromDto(profileDtoList);

        List<Profile> failed = new ArrayList<>();
        profileValidator.validate(profiles, failed, dto.getStatus().getErrors());
        dto.getFail().addAll(profileMapper.mapToDto(failed));

        dto.getSuccess().addAll(profileMapper.mapToDto(saveAllProfiles(profiles)));
        if(dto.getStatus().getErrors().size() > 0) {
            dto.getStatus().setErrorMessage("Validation failed. " + dto.getStatus().getErrors().size() + " error(s)");
        }
        return dto;
    }

    private Profile saveOrUpdateProfile(Profile profile) {

        if(checkProfileExists(profile)) {
            Profile updateProfile = getProfileByProfileIdentity(profile.getProfileIdentity());
            updateProfile.setRatio(profile.getRatio());
            return profileRepository.save(updateProfile);
        } else {
            return profileRepository.save(profile);
        }
    }

    private boolean checkProfileExists(Profile profile) {

        Profile checkProfile = getProfileByProfileIdentity(profile.getProfileIdentity());
        return checkProfile != null;
    }
}
