package com.jobinterview.meterreadings.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.dto.ReadingSaveUpdateDto;

public interface ReadingService {

    List<ReadingDto> getReadings(Map<String, String> searchParams);

    Reading getReadingByReadingIdentity(ReadingIdentity readingIdentity);

    List<Reading> saveAllReadings(List<Reading> readings, List<Reading> failed, List<String> errors);

    int deleteReadingsByConnectionAndYear(Long connectionId, Integer year);

    Long getConsumptionForConnectionAndMonth(Long connectionId, Date date);

    ReadingSaveUpdateDto saveAllReadingDtos(List<ReadingDto> readingDtos);
}
