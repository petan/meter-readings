package com.jobinterview.meterreadings.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.dto.ReadingSaveUpdateDto;
import com.jobinterview.meterreadings.service.ReadingService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/readings", produces = "application/json")
public class ReadingController {

    private final ReadingService readingService;

    public ReadingController(ReadingService readingService) {
        this.readingService = readingService;
    }

    @GetMapping
    public List<ReadingDto> getAllReadings(@RequestParam Map<String, String> requestParams) {

        return readingService.getReadings(requestParams);
    }

    @GetMapping(value = "/consumption/connection/{connectionId}/month/{month}")
    public ResponseEntity getConsumption(@PathVariable(value = "connectionId") Long connectionId,
                               @PathVariable(value = "month") @DateTimeFormat(pattern = "yyyyMM") Date month) {

        Long consumption = readingService.getConsumptionForConnectionAndMonth(connectionId, month);
        if(consumption != null) {
            return ResponseEntity.ok().body(consumption);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping
    public ResponseEntity<ReadingSaveUpdateDto> saveReadings(@RequestBody List<ReadingDto> readingDtoList) {

        ReadingSaveUpdateDto dto = readingService.saveAllReadingDtos(readingDtoList);

        if (dto.getStatus().getErrors().size() > 0) {
            // if some data didn't pass validation - code 400 - bad request will be sent, no matter that some of the data is successfully saved
            // I couldn't find some status code for Partial data stored or something. Response contains a list of success and fail data and errors list.
            return ResponseEntity.badRequest().body(dto);
        } else {
            return ResponseEntity.ok(dto);
        }
    }

    @DeleteMapping()
    public ResponseEntity deleteReadingsByYear(@RequestParam(value = "year") Integer year,
                                               @RequestParam(value = "connectionId", required = false) Long connectionId) {

        int result = readingService.deleteReadingsByConnectionAndYear(connectionId, year);

        if(result > 0) {
            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
