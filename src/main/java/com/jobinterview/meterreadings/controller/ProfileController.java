package com.jobinterview.meterreadings.controller;

import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ProfileSaveUpdateDto;
import com.jobinterview.meterreadings.mapper.ProfileMapper;
import com.jobinterview.meterreadings.service.ProfileService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping(value = "/api/profiles", produces = "application/json")
public class ProfileController {

    private final ProfileService profileService;
    private final ProfileMapper profileMapper;

    public ProfileController(ProfileService profileService, ProfileMapper profileMapper) {
        this.profileService = profileService;
        this.profileMapper = profileMapper;
    }

    @GetMapping()
    public List<ProfileDto> getAllProfiles(@RequestParam Map<String, String> requestParams) {

        return profileService.getProfiles(requestParams);
    }

    @PostMapping()
    public ResponseEntity<ProfileSaveUpdateDto> saveProfileRatios(@RequestBody List<ProfileDto> profileDtoList) {

        ProfileSaveUpdateDto dto = profileService.saveAllProfileDtos(profileDtoList);

        if (dto.getStatus().getErrors().size() > 0) {
            // if some data didn't pass validation - code 400 - bad request will be sent, no matter that some of the data is successfully saved
            // I couldn't find some status code for Partial data stored or something. Response contains a list of success and fail data and errors list.
            return ResponseEntity.badRequest().body(dto);
        } else {
            return ResponseEntity.ok(dto);
        }
    }

    @PutMapping(value = "/year/{year}")
    public ResponseEntity<ProfileSaveUpdateDto> updateProfileRatiosByYear(@PathVariable(value = "year") Integer year,
                                                    @RequestBody List<ProfileDto> profileDTOList) {

        profileMapper.updateDtoWithYear(profileDTOList, year);

        ProfileSaveUpdateDto dto = profileService.saveAllProfileDtos(profileDTOList);

        if (dto.getStatus().getErrors().size() > 0) {
            return ResponseEntity.badRequest().body(dto);
        } else {
            return ResponseEntity.ok(dto);
        }
    }

    @DeleteMapping()
    public ResponseEntity deleteProfilesByYear(@RequestParam(value = "year") Integer year,
                                               @RequestParam(value = "name", required = false) String name) {

        int result = profileService.deleteProfilesByNameAndYear(name, year);

        if(result > 0) {
            return ResponseEntity.ok().body(result);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
