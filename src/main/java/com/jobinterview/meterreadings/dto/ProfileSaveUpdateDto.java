package com.jobinterview.meterreadings.dto;

import java.util.ArrayList;
import java.util.List;

import com.jobinterview.meterreadings.validator.ValidationStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProfileSaveUpdateDto {

    List<ProfileDto> success = new ArrayList<>();
    List<ProfileDto> fail = new ArrayList<>();
    ValidationStatus status = new ValidationStatus("OK");
}
