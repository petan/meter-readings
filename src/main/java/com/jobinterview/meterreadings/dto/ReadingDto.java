package com.jobinterview.meterreadings.dto;

public class ReadingDto {
    private Long connectionID;
    private String profileName;
    private String month;
    private Integer year;
    private Long reading;

    public ReadingDto(Long connectionID, String profileName, String month, Integer year, Long reading) {
        this.connectionID = connectionID;
        this.profileName = profileName;
        this.month = month;
        this.year = year;
        this.reading = reading;
    }

    public ReadingDto(Long connectionID, String profileName, String month, Long reading) {
        this.connectionID = connectionID;
        this.profileName = profileName;
        this.month = month;
        this.reading = reading;
    }

    public ReadingDto() {

    }

    public Long getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(Long connectionID) {
        this.connectionID = connectionID;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Long getReading() {
        return reading;
    }

    public void setReading(Long reading) {
        this.reading = reading;
    }
}
