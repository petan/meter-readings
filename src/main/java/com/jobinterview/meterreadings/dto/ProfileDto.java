package com.jobinterview.meterreadings.dto;

public class ProfileDto {
    private String name;
    private String month;
    private Integer year;
    private Double ratio;

    public ProfileDto(String name, String month, Integer year, Double ratio) {
        this.name = name;
        this.month = month;
        this.year = year;
        this.ratio = ratio;
    }

    public ProfileDto(String name, String month, Double ratio) {
        this.name = name;
        this.month = month;
        this.ratio = ratio;
    }

    public ProfileDto() {

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }
}
