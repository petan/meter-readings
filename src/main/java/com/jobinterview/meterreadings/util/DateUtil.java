package com.jobinterview.meterreadings.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

public class DateUtil
{
    private static Logger log = LoggerFactory.getLogger(DateUtil.class);

    public static Date dateFromYearAndMonth(Integer year, String month) {
        int tempYear;
        int tempMonth;
        if(year == null) {
            Calendar cal = Calendar.getInstance();
            tempYear = cal.get(Calendar.YEAR);
        } else {
            tempYear = year;
        }

        try {
            Date date = new SimpleDateFormat("MMM", Locale.ENGLISH).parse(StringUtils.capitalize(month.toLowerCase()));
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            tempMonth = cal.get(Calendar.MONTH);
        } catch(ParseException e) {
            log.error("Month format is incorrect: " + e);
            return null;
        }

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, tempYear);
        cal.set(Calendar.MONTH, tempMonth);
        cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
        cal.set(Calendar.HOUR_OF_DAY, cal.getActualMaximum(Calendar.HOUR_OF_DAY));
        cal.set(Calendar.MINUTE, cal.getActualMaximum(Calendar.MINUTE));
        cal.set(Calendar.SECOND, cal.getActualMaximum(Calendar.SECOND));
        cal.set(Calendar.MILLISECOND, cal.getActualMaximum(Calendar.MILLISECOND));

        return cal.getTime();
    }

    public static String monthFromDate(Date date, String format) {
        return new SimpleDateFormat(format, Locale.ENGLISH).format(date);
    }

    public static int yearFromDate(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static boolean isValidMonth(String month) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("MMM", Locale.ENGLISH);
            sdf.setLenient(false);
            sdf.parse(StringUtils.capitalize(month.toLowerCase()));
        } catch(ParseException e) {
            return false;
        }
        return true;
    }

    public static boolean compareDates(Date d1, Date d2) {

        Calendar c1 = Calendar.getInstance();
        c1.setTime(d1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(d2);

        return c1.compareTo(c2)==0;
    }

    /***
     * This method returns same representation of date no matter if parameter passed is Timestamp or Date type.
     * This is mostly used in entity classes because hibernate returns Timestamp values which fail equals and compareTo
     * checks against Date value
     * @param d date of Date or Timestamp type
     * @return Date representation that can be correctly compared
     */
    public static Date returnDate(Date d) {

        try{
            Calendar c = Calendar.getInstance();
            c.setTime(d);
            return c.getTime();
        } catch (Exception e) {
            log.error("Error when returning date.");
            return null;
        }
    }

    public static Date addMonths(Date date, int amount) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);

        c.add(Calendar.MONTH, amount);

        return c.getTime();
    }
}
