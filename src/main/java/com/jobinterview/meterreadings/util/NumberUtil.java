package com.jobinterview.meterreadings.util;

public class NumberUtil {

    public static boolean isDouble(String text) {
        try {
            Double.parseDouble(text);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
