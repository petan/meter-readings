package com.jobinterview.meterreadings.util;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.jobinterview.meterreadings.validator.ValidationStatus;

public class ResponseBuilder<T> {
    @JsonProperty("success")
    private List<T> successList = new ArrayList<>();

    @JsonProperty("fail")
    private List<T> failureList = new ArrayList<>();

    @JsonProperty("status")
    private ValidationStatus validationStatus;

    public ResponseBuilder(List<T> success, ValidationStatus validationStatus) {
        this.successList.addAll(success);
        this.validationStatus = validationStatus;
    }

    public ResponseBuilder(List<T> successList, List<T> failureList, ValidationStatus validationStatus) {
        this.successList.addAll(successList);
        this.failureList.addAll(failureList);
        this.validationStatus = validationStatus;
    }

    public List<T> getSuccess() {
        return successList;
    }

    public void setSuccessList(List<T> successList) {
        this.successList = successList;
    }

    public List<T> getFailureList() {
        return failureList;
    }

    public void setFailureList(List<T> failureList) {
        this.failureList = failureList;
    }

    public ValidationStatus getValidationStatus() {
        return validationStatus;
    }

    public void setValidationStatus(ValidationStatus validationStatus) {
        this.validationStatus = validationStatus;
    }
}
