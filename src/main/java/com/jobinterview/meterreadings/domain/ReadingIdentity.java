package com.jobinterview.meterreadings.domain;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.jobinterview.meterreadings.util.DateUtil;

@Embeddable
public class ReadingIdentity implements Serializable {

    @NotNull
    private Long connectionID;

    @NotNull
    private Date date;

    public Long getConnectionID() {
        return connectionID;
    }

    public void setConnectionID(Long connectionID) {
        this.connectionID = connectionID;
    }

    public Date getDate() {
        return DateUtil.returnDate(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public ReadingIdentity() {

    }

    public ReadingIdentity(Long connectionID, Date date) {
        this.connectionID = connectionID;
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReadingIdentity that = (ReadingIdentity) o;
        return Objects.equals(getConnectionID(), that.getConnectionID()) &&
                DateUtil.compareDates(getDate(), that.getDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getConnectionID(), getDate());
    }
}
