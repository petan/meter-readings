package com.jobinterview.meterreadings.domain;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "profile")
public class Profile {

    @EmbeddedId
    @Valid
    private ProfileIdentity profileIdentity;

    @NotNull
    private Double ratio;

    public Profile() {

    }

    public Profile(ProfileIdentity profileIdentity, Double ratio) {
        this.profileIdentity = profileIdentity;
        this.ratio = ratio;
    }

    public ProfileIdentity getProfileIdentity() {
        return profileIdentity;
    }

    public void setProfileIdentity(ProfileIdentity profileIdentity) {
        this.profileIdentity = profileIdentity;
    }

    public Double getRatio() {
        return ratio;
    }

    public void setRatio(Double ratio) {
        this.ratio = ratio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Profile profile = (Profile) o;
        return Objects.equals(getProfileIdentity(), profile.getProfileIdentity()) &&
                Objects.equals(getRatio(), profile.getRatio());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getProfileIdentity(), getRatio());
    }
}
