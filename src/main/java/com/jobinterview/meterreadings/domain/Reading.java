package com.jobinterview.meterreadings.domain;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "reading")
public class Reading {

    @EmbeddedId
    @Valid
    ReadingIdentity readingIdentity;

    @NotBlank
    @Column(nullable = false)
    private String profileName;

    @NotNull
    private Long reading;

    public Reading() {

    }

    public Reading(ReadingIdentity readingIdentity, String profileName, Long reading) {
        this.readingIdentity = readingIdentity;
        this.profileName = profileName;
        this.reading = reading;
    }

    public ReadingIdentity getReadingIdentity() {
        return readingIdentity;
    }

    public void setReadingIdentity(ReadingIdentity readingIdentity) {
        this.readingIdentity = readingIdentity;
    }

    public String getProfileName() {
        return profileName;
    }

    public void setProfileName(String profileName) {
        this.profileName = profileName;
    }

    public Long getReading() {
        return reading;
    }

    public void setReading(Long reading) {
        this.reading = reading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reading reading1 = (Reading) o;
        return Objects.equals(getReadingIdentity(), reading1.getReadingIdentity()) &&
                Objects.equals(getProfileName(), reading1.getProfileName()) &&
                Objects.equals(getReading(), reading1.getReading());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getReadingIdentity(), getProfileName(), getReading());
    }
}
