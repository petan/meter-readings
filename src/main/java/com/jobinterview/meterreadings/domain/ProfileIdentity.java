package com.jobinterview.meterreadings.domain;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

import com.jobinterview.meterreadings.util.DateUtil;

@Embeddable
public class ProfileIdentity implements Serializable {
    @NotBlank
    private String name;

    @NotNull
    private Date date;

    public ProfileIdentity() {

    }

    public ProfileIdentity(String name, Date date) {
        this.name = name;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDate() {
        return DateUtil.returnDate(date);
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileIdentity that = (ProfileIdentity) o;
        return Objects.equals(getName(), that.getName()) &&
                DateUtil.compareDates(getDate(), that.getDate());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getName(), getDate());
    }
}
