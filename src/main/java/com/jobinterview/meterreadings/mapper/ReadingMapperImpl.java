package com.jobinterview.meterreadings.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.util.DateUtil;
import org.springframework.stereotype.Component;

@Component
public class ReadingMapperImpl implements ReadingMapper {

    @Override
    public ReadingDto mapToDto(Reading reading) {
        ReadingDto readingDto = new ReadingDto();

        readingDto.setConnectionID(reading.getReadingIdentity().getConnectionID());
        readingDto.setProfileName(reading.getProfileName());
        readingDto.setReading(reading.getReading());
        readingDto.setMonth(DateUtil.monthFromDate(reading.getReadingIdentity().getDate(), "MMM"));
        readingDto.setYear(DateUtil.yearFromDate(reading.getReadingIdentity().getDate()));

        return readingDto;
    }

    @Override
    public List<ReadingDto> mapToDto(List<Reading> readings) {
        if (readings != null && readings.size() > 0) {
            return readings.stream().map(reading -> mapToDto(reading)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public Reading mapFromDto(ReadingDto readingDto) {
        ReadingIdentity readingIdentity = new ReadingIdentity();
        Reading reading = new Reading();

        readingIdentity.setConnectionID(readingDto.getConnectionID());
        readingIdentity.setDate(DateUtil.dateFromYearAndMonth(readingDto.getYear(), readingDto.getMonth()));

        reading.setReadingIdentity(readingIdentity);
        reading.setProfileName(readingDto.getProfileName());
        reading.setReading(readingDto.getReading());

        return reading;
    }

    @Override
    public List<Reading> mapFromDto(List<ReadingDto> readingDtos) {
        if (readingDtos != null && readingDtos.size() > 0) {
            return readingDtos.stream().map(readingDto -> mapFromDto(readingDto)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }
}
