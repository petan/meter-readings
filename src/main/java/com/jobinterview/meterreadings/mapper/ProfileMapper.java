package com.jobinterview.meterreadings.mapper;

import java.util.List;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.dto.ProfileDto;

public interface ProfileMapper {

    ProfileDto mapToDto(Profile profile);
    List<ProfileDto> mapToDto(List<Profile> profiles);
    Profile mapFromDto(ProfileDto profileDto);
    List<Profile> mapFromDto(List<ProfileDto> profileDtos);
    void updateDtoWithYear(List<ProfileDto> profileDtos, Integer year);
}
