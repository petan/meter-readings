package com.jobinterview.meterreadings.mapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.util.DateUtil;
import org.springframework.stereotype.Component;

@Component
public class ProfileMapperImpl implements ProfileMapper {

    @Override
    public ProfileDto mapToDto(Profile profile) {
        ProfileDto profileDto = new ProfileDto();

        profileDto.setName(profile.getProfileIdentity().getName());
        profileDto.setRatio(profile.getRatio());
        profileDto.setMonth(DateUtil.monthFromDate(profile.getProfileIdentity().getDate(), "MMM"));
        profileDto.setYear(DateUtil.yearFromDate(profile.getProfileIdentity().getDate()));

        return profileDto;
    }

    @Override
    public List<ProfileDto> mapToDto(List<Profile> profiles) {
        if(profiles != null && profiles.size() > 0) {
            return profiles.stream().map(profile -> mapToDto(profile)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }


    @Override
    public Profile mapFromDto(ProfileDto profileDto) {
        ProfileIdentity profileIdentity = new ProfileIdentity();
        Profile profile = new Profile();
        profileIdentity.setName(profileDto.getName());
        profileIdentity.setDate(DateUtil.dateFromYearAndMonth(profileDto.getYear(), profileDto.getMonth()));

        profile.setProfileIdentity(profileIdentity);
        profile.setRatio(profileDto.getRatio());

        return profile;
    }

    @Override
    public List<Profile> mapFromDto(List<ProfileDto> profileDtos) {
        if(profileDtos != null && profileDtos.size() > 0) {
            return profileDtos.stream().map(profileDto -> mapFromDto(profileDto)).collect(Collectors.toList());
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public void updateDtoWithYear(List<ProfileDto> profileDtos, Integer year) {
        profileDtos.forEach(profileDto -> profileDto.setYear(year));
    }
}
