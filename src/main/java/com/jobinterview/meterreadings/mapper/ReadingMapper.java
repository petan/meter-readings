package com.jobinterview.meterreadings.mapper;

import java.util.List;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.dto.ReadingDto;

public interface ReadingMapper {

    ReadingDto mapToDto(Reading reading);
    List<ReadingDto> mapToDto(List<Reading> readings);
    Reading mapFromDto(ReadingDto readingDto);
    List<Reading> mapFromDto(List<ReadingDto> readingDtos);
}
