package com.jobinterview.meterreadings.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Reading;

public interface ReadingRepositoryCustom {

    int deleteReadingsByConnectionAndYear(Long connectionId, Integer year);

    List<Reading> findReadings(Map<String, String> searchParams);

    Reading getReadingForConnectionAndMonth(Long connectionId, Date date);
}
