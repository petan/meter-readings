package com.jobinterview.meterreadings.repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class ReadingRepositoryCustomImpl implements ReadingRepositoryCustom {

    @Autowired
    EntityManager entityManager;

    @Override
    @Transactional
    public int deleteReadingsByConnectionAndYear(Long connectionId, Integer year) {
        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("year", year);

        String sql = "DELETE FROM Reading r WHERE YEAR(r.readingIdentity.date) = :year ";
        if(connectionId != null) {
            sql += "AND r.readingIdentity.connectionID = :connectionId";
            parameterMap.put("connectionId", connectionId);
        }

        Query query = entityManager.createQuery(sql);
        parameterMap.forEach((key, value) -> query.setParameter(key, value));

        return query.executeUpdate();
    }

    @Override
    public List<Reading> findReadings(Map<String, String> searchParams) {

        Map<String, Object> validParameterMap = new HashMap<>();
        String sql = "SELECT r FROM Reading r WHERE 1=1 ";

        for(Map.Entry<String, String> param : searchParams.entrySet() ) {
            if(param.getKey().equals("connectionId") && param.getValue() != null && !param.getValue().isEmpty()) {
                sql += " AND r.readingIdentity.connectionID = :connectionId ";
                validParameterMap.put(param.getKey(), Long.parseLong(param.getValue()));
            }
            if(param.getKey().equals("year") && param.getValue() != null && !param.getValue().isEmpty()) {
                sql += " AND YEAR(r.readingIdentity.date) = :year ";
                validParameterMap.put(param.getKey(), Integer.parseInt(param.getValue()));
            }
        }
        sql += " ORDER BY r.readingIdentity.connectionID, r.readingIdentity.date ";

        Query query = entityManager.createQuery(sql);
        validParameterMap.forEach((key, value) -> query.setParameter(key, value));

        return query.getResultList();
    }

    @Override
    public Reading getReadingForConnectionAndMonth(Long connectionId, Date date) {

        String sql = "SELECT r FROM Reading r " +
                " WHERE r.readingIdentity.connectionID = :connectionId " +
                " AND YEAR(r.readingIdentity.date) = :year " +
                " AND MONTH(r.readingIdentity.date) = :month ";

        Query query = entityManager.createQuery(sql);
        query.setParameter("connectionId", connectionId);
        query.setParameter("year", DateUtil.yearFromDate(date));
        query.setParameter("month", Integer.parseInt(DateUtil.monthFromDate(date, "M")));

        return (Reading)query.getResultList().stream().findFirst().orElse(null);
    }
}
