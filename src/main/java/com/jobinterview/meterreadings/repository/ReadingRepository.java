package com.jobinterview.meterreadings.repository;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReadingRepository extends JpaRepository<Reading, Long>, ReadingRepositoryCustom {

    Reading findByReadingIdentity(ReadingIdentity readingIdentity);
}
