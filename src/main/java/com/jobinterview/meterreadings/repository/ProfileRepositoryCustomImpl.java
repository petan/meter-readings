package com.jobinterview.meterreadings.repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Profile;
import org.springframework.beans.factory.annotation.Autowired;

public class ProfileRepositoryCustomImpl implements ProfileRepositoryCustom {

    @Autowired
    EntityManager entityManager;

    @Override
    public List<Profile> findProfiles(Map<String, String> searchParams) {

        Map<String, Object> validParameterMap = new HashMap<>();
        String sql = "SELECT p FROM Profile p WHERE 1=1 ";

        for(Map.Entry<String, String> param : searchParams.entrySet() ) {
            if(param.getKey().equals("name") && param.getValue() != null) {
                sql += " AND p.profileIdentity.name = :name ";
                validParameterMap.put(param.getKey(), param.getValue());
            }
            if(param.getKey().equals("year") && param.getValue() != null && !param.getValue().isEmpty()) {
                sql += " AND YEAR(p.profileIdentity.date) = :year ";
                validParameterMap.put(param.getKey(), Integer.parseInt(param.getValue()));
            }
        }
        sql += " ORDER BY p.profileIdentity.name, p.profileIdentity.date ";

        Query query = entityManager.createQuery(sql);
        validParameterMap.forEach((key, value) -> query.setParameter(key, value));

        return query.getResultList();
    }

    @Override
    public List<Profile> findProfilesByNameListAndYear(List<String> nameList, Integer year) {
        String sql = "SELECT p FROM Profile p WHERE YEAR(p.profileIdentity.date) = :year " +
                " AND p.profileIdentity.name IN :nameList " +
                " ORDER BY p.profileIdentity.name, p.profileIdentity.date ";
        Query query = entityManager.createQuery(sql);
        query.setParameter("year", year);
        query.setParameter("nameList", nameList);

        return query.getResultList();
    }

    @Override
    @Transactional
    public int deleteProfilesByNameAndYear(String name, Integer year) {

        Map<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("year", year);

        String sql = "DELETE FROM Profile p WHERE YEAR(p.profileIdentity.date) = :year ";
        if(name != null && !name.isEmpty()) {
            sql += "AND p.profileIdentity.name = :name";
            parameterMap.put("name", name);
        }

        Query query = entityManager.createQuery(sql);
        parameterMap.forEach((key, value) -> query.setParameter(key, value));

        return query.executeUpdate();
    }
}
