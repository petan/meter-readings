package com.jobinterview.meterreadings.repository;

import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.domain.Profile;

public interface ProfileRepositoryCustom {

    int deleteProfilesByNameAndYear(String name, Integer year);

    List<Profile> findProfiles(Map<String, String> searchParams);

    List<Profile> findProfilesByNameListAndYear(List<String> nameList, Integer year);
}
