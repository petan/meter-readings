package com.jobinterview.meterreadings.validator;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.stream.Collectors;

import com.jobinterview.meterreadings.domain.Profile;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProfileValidator {

    public boolean validate(Profile profile, List<String> errors) {
        int errorCount = 0;

        if (profile.getProfileIdentity().getName() == null || profile.getProfileIdentity().getName().isEmpty()) {
            errors.add("name empty");
            errorCount++;
        }
        if (profile.getProfileIdentity().getDate() == null) {
            errors.add("date not valid");
            errorCount++;
        }
        if (profile.getRatio() == null || profile.getRatio() < 0 || profile.getRatio() > 1) {
            errors.add("ratio empty or invalid range");
            errorCount++;
        }
        if (errorCount > 0)
            return false;
        else
            return true;
    }

    public void validate(List<Profile> profiles, List<Profile> fail, List<String> errors) {
        ListIterator<Profile> iterator = profiles.listIterator();
        while (iterator.hasNext()) {
            Profile temp = iterator.next();
            if (!validate(temp, errors)) {
                log.debug("found failed validation: removing element: " + temp.toString());
                fail.add(temp);
                iterator.remove();
            }
        }

        logicalProfilesValidation(profiles, fail, errors);
    }

    private void logicalProfilesValidation(List<Profile> profiles, List<Profile> fail, List<String> errors) {

        Map<String, List<Profile>> ratiosPerProfile = profiles.stream()
                .collect(Collectors.groupingBy(profile -> profile.getProfileIdentity().getName()));

        for(Iterator<Map.Entry<String, List<Profile>>> iter = ratiosPerProfile.entrySet().iterator(); iter.hasNext();) {
            Map.Entry<String, List<Profile>> entry = iter.next();

            Double sumRatio = entry.getValue().stream().mapToDouble(Profile::getRatio).sum();
            log.debug("Key: " + entry.getKey() + " Sum ratio: " + sumRatio);
            if(sumRatio != 1.0) {
                errors.add("Profile: " + entry.getKey() + " ratio sum is not 1.0");
                fail.addAll(entry.getValue());
                iter.remove();
            }
        }

        // replace profiles list with validated profiles only
        buildValidatedList(profiles, ratiosPerProfile);
    }

    private void buildValidatedList(List<Profile> profiles, Map<String, List<Profile>> ratiosPerProfile) {
        profiles.clear();
        ratiosPerProfile.values().forEach(list -> profiles.addAll(list));
    }
}
