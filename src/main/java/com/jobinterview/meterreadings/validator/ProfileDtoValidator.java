package com.jobinterview.meterreadings.validator;

import java.util.List;
import java.util.ListIterator;

import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ProfileDtoValidator {

    public boolean validate(ProfileDto profileDto, List<String> errors) {
        int errorCount = 0;

        if (profileDto.getName() == null || profileDto.getName().isEmpty()) {
            errors.add("name empty");
            errorCount++;
        }
        if (profileDto.getMonth() == null || profileDto.getMonth().isEmpty() || !DateUtil.isValidMonth(profileDto.getMonth())) {
            errors.add("month empty or incorrect format");
            errorCount++;
        }
        if (profileDto.getRatio() == null || profileDto.getRatio() < 0 || profileDto.getRatio() > 1) {
            errors.add("ratio empty or invalid range");
            errorCount++;
        }
        if (errorCount > 0)
            return false;
        else
            return true;
    }

    public void validate(List<ProfileDto> profileDtos, List<ProfileDto> fail, List<String> errors) {
        ListIterator<ProfileDto> iterator = profileDtos.listIterator();
        while (iterator.hasNext()) {
            ProfileDto temp = iterator.next();
            if (!validate(temp, errors)) {
                log.debug("found failed validation: removing element: " + temp.toString());
                fail.add(temp);
                iterator.remove();
            }
        }
    }
}
