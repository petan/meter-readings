package com.jobinterview.meterreadings.validator;

import java.util.List;
import java.util.ListIterator;

import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ReadingDtoValidator {

    public boolean validate(ReadingDto readingDto, List<String> errors) {
        int errorCount = 0;

        if (readingDto.getConnectionID() == null) {
            errors.add("connection ID empty");
            errorCount++;
        }
        if (readingDto.getProfileName() == null || readingDto.getProfileName().isEmpty()) {
            errors.add("profileName empty");
            errorCount++;
        }
        if (readingDto.getMonth() == null || readingDto.getMonth().isEmpty() || !DateUtil.isValidMonth(readingDto.getMonth())) {
            errors.add("month empty or incorrect format");
            errorCount++;
        }
        if (readingDto.getReading() == null) {
            errors.add("reading empty or invalid range");
            errorCount++;
        }
        if (errorCount > 0)
            return false;
        else
            return true;
    }

    public void validate(List<ReadingDto> readingDtos, List<ReadingDto> fail, List<String> errors) {
        ListIterator<ReadingDto> iterator = readingDtos.listIterator();
        while (iterator.hasNext()) {
            ReadingDto temp = iterator.next();
            if (!validate(temp, errors)) {
                log.debug("found failed validation: removing element: " + temp.toString());
                fail.add(temp);
                iterator.remove();
            }
        }
    }
}
