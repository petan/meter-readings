package com.jobinterview.meterreadings.validator;

import java.util.*;
import java.util.stream.Collectors;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.service.ProfileService;
import com.jobinterview.meterreadings.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ReadingValidator {

    private final ProfileService profileService;

    public ReadingValidator(ProfileService profileService) {
        this.profileService = profileService;
    }

    public boolean validate(Reading reading, List<String> errors) {
        int errorCount = 0;

        if (reading.getReadingIdentity().getConnectionID() == null) {
            errors.add("connection ID empty");
            errorCount++;
        }
        if (reading.getReadingIdentity().getDate() == null) {
            errors.add("date not valid");
            errorCount++;
        }
        if (reading.getProfileName() == null || reading.getProfileName().isEmpty()) {
            errors.add("name empty");
            errorCount++;
        }
        if (reading.getReading() == null) {
            errors.add("reading empty");
            errorCount++;
        }
        if (errorCount > 0)
            return false;
        else
            return true;
    }

    public void validate(List<Reading> readings, List<Reading> fail, List<String> errors) {
        ListIterator<Reading> iterator = readings.listIterator();
        while (iterator.hasNext()) {
            Reading temp = iterator.next();
            if (!validate(temp, errors)) {
                log.debug("found failed validation: removing element: " + temp.toString());
                fail.add(temp);
                iterator.remove();
            }
        }

        logicalReadingsValidation(readings, fail, errors);
    }

    private void logicalReadingsValidation(List<Reading> readings, List<Reading> fail, List<String> errors) {

        Integer currentYear = Calendar.getInstance().get(Calendar.YEAR); //todo assume current year for now
        List<Profile> profileList = profileService.getSortedListOfRelevantProfiles(readings, currentYear);

        // returns lists of readings sorted by month(date) and grouped by connectionId
        Map<Long, List<Reading>> readingsPerConnection = readings.stream()
                .sorted(Comparator.comparing(reading -> reading.getReadingIdentity().getDate()))
                .collect(Collectors.groupingBy(reading -> reading.getReadingIdentity().getConnectionID()));

        // iterate over each connection and remove invalid ones. At the end, only valid connections will remain in the map
        for(Iterator<Map.Entry<Long, List<Reading>>> iter = readingsPerConnection.entrySet().iterator(); iter.hasNext();) {
            Map.Entry<Long, List<Reading>> entry = iter.next();

            // todo: implement previous year last consumption maybe
            // this is valid when treating every year for itself (consumption starts from 0). For simplicity, I will leave this like it is for this task.
            // in real life scenario, I would find last consumption of previous year and start from there or assume zero if it doesn't exist
            Double totalYearConsumptionForConnection = entry.getValue().get(entry.getValue().size()-1).getReading().doubleValue();

            Long lastReading = 0L;
            for(int i = 0; i < entry.getValue().size(); i++) {

                // validating that previous month doesn't have larger reading amount
                Reading r = entry.getValue().get(i);
                log.debug("connection " + r.getReadingIdentity().getConnectionID() + " - " + r.getReadingIdentity().getDate() + " - " + r.getReading());
                if(!validatePreviousMonthConsumption(entry.getValue().get(i), lastReading, errors)) {
                    fail.addAll(entry.getValue()); //add complete list of readings for false connectionId
                    iter.remove();
                    break;
                }

                // validating that profile ratio exists for current connection and month
                Profile thisMonthRatio = getProfileRatioForConnectionAndDate(r, profileList);
                if(!validateProfileRatioExists(r, thisMonthRatio, errors)) {
                    fail.addAll(entry.getValue()); //add complete list of readings for false connectionId
                    iter.remove();
                    break;
                }

                // validating that consumption for each month is inside tolerance levels based on ratio
                Double expected = totalYearConsumptionForConnection * thisMonthRatio.getRatio();
                Long consumption = r.getReading() - lastReading;
                if(!validateConsumptionInsideTolerance(r, expected, consumption, errors)) {
                    fail.addAll(entry.getValue()); //add complete list of readings for false connectionId
                    iter.remove();
                    break;
                }

                lastReading = r.getReading();
            }
        }

        // replace readings list with validated readings only
        buildValidatedList(readings, readingsPerConnection);
    }

    private boolean validatePreviousMonthConsumption(Reading r, Long lastReading, List<String> errors) {

        if(lastReading > r.getReading()) {
            errors.add("ConnectionId: " + r.getReadingIdentity().getConnectionID() + " reading for month: "
                    + DateUtil.monthFromDate(r.getReadingIdentity().getDate(), "MMM") + " validation fail, can't be lower than previous month");

            return false;
        } else {
            return true;
        }
    }

    private Profile getProfileRatioForConnectionAndDate(Reading r, List<Profile> profileList) {
        ProfileIdentity profileIdentity = new ProfileIdentity(r.getProfileName(), r.getReadingIdentity().getDate());
        return profileList.stream().filter(profile -> profile.getProfileIdentity().equals(profileIdentity)).findFirst().orElse(null);
    }

    private boolean validateProfileRatioExists(Reading r, Profile thisMonthRatio, List<String> errors) {
        if(thisMonthRatio == null) {
            errors.add("ConnectionId: " + r.getReadingIdentity().getConnectionID() + " reading for month: "
                    + DateUtil.monthFromDate(r.getReadingIdentity().getDate(), "MMM") + " validation fail, ratio for corresponding month does not exist");

            return false;
        } else {
            return true;
        }
    }

    private boolean validateConsumptionInsideTolerance(Reading r, Double expected, Long consumption, List<String> errors) {
        if(consumption < expected * 0.75 || consumption > expected * 1.25) {
            errors.add("ConnectionId: " + r.getReadingIdentity().getConnectionID() + " reading for month: "
                    + DateUtil.monthFromDate(r.getReadingIdentity().getDate(), "MMM") + " validation fail, consumption exceeds expected values");

            return false;
        } else {
            return true;
        }
    }

    private void buildValidatedList(List<Reading> readings, Map<Long, List<Reading>> readingsPerConnection) {
        readings.clear();
        readingsPerConnection.values().forEach(list -> readings.addAll(list));
    }
}
