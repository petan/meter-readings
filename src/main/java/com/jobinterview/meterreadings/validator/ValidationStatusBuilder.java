package com.jobinterview.meterreadings.validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

public class ValidationStatusBuilder {

    public static ValidationStatus fromBindingErrors(Errors errors) {
        ValidationStatus status = new ValidationStatus("OK");
        if(errors.getErrorCount() > 0)
            status.setErrorMessage("Validation failed. " + errors.getErrorCount() + " error(s)");
        for (ObjectError objectError : errors.getAllErrors()) {
            status.addValidationError(objectError.getDefaultMessage());
        }
        return status;
    }
}
