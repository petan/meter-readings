package com.jobinterview.meterreadings.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.dto.ProfileDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileMapperImplTest {

    @Autowired
    ProfileMapper profileMapper;

    @Test
    public void mapToDto_success() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2018");
        ProfileIdentity profileIdentity = new ProfileIdentity("test_profile", date);
        Profile profile = new Profile(profileIdentity, 0.25);

        ProfileDto profileDto = profileMapper.mapToDto(profile);

        assertEquals("Checking name:", "test_profile", profileDto.getName());
        assertEquals("Checking month:", "Mar", profileDto.getMonth());
        assertTrue("Checking year:", 2018 == profileDto.getYear());
        assertTrue("Checking ratio:", 0.25 == profileDto.getRatio());
    }

    @Test
    public void mapToDtoList_success() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("31/07/2018");
        ProfileIdentity profileIdentity = new ProfileIdentity("test_profile", date);
        Profile profile = new Profile(profileIdentity, 0.25);

        List<Profile> profiles = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            profiles.add(profile);
        }
        List<ProfileDto> profileDtos = profileMapper.mapToDto(profiles);

        assertTrue("Checking element count: ", 12 == profileDtos.size());
        profileDtos.forEach(profileDto -> {
            assertEquals("Checking name:", "test_profile", profileDto.getName());
            assertEquals("Checking month:", "Jul", profileDto.getMonth());
            assertTrue("Checking year:", 2018 == profileDto.getYear());
            assertTrue("Checking ratio:", 0.25 == profileDto.getRatio());
        });
    }

    @Test
    public void mapFromDto_withCurrentYear_success() throws Exception {

        ProfileDto profileDto = new ProfileDto();
        profileDto.setName("test_profile");
        profileDto.setMonth("APR");
        profileDto.setYear(2019);
        profileDto.setRatio(0.65);

        Profile profile = profileMapper.mapFromDto(profileDto);
        Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("30/04/2019 23:59:59.999");

        assertEquals("Checking name:", "test_profile", profile.getProfileIdentity().getName());
        assertEquals("Checking date:", date, profile.getProfileIdentity().getDate());
        assertTrue("Checking ratio:", 0.65 == profile.getRatio());
    }

    @Test
    public void mapFromDtoList_withoutCurrentYear_success() throws Exception {

        ProfileDto profileDto = new ProfileDto();
        profileDto.setName("test_profile");
        profileDto.setMonth("APR");
        profileDto.setRatio(0.65);

        List<ProfileDto> profileDtos = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            profileDtos.add(profileDto);
        }
        List<Profile> profiles = profileMapper.mapFromDto(profileDtos);

        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR); //without year, system time year is assumed
        String stringDate = "30/04/" + currentYear + " 23:59:59.999";
        Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse(stringDate);

        assertTrue("Checking element count: ", 12 == profiles.size());
        profiles.forEach(p -> {
            assertEquals("Checking name:", "test_profile", p.getProfileIdentity().getName());
            assertEquals("Checking month:", date, p.getProfileIdentity().getDate());
            assertTrue("Checking ratio:", 0.65 == p.getRatio());
        });
    }

    @Test
    public void updateDtoWithYear_success() throws Exception {
        Integer year = 2018;
        ProfileDto profileDto = new ProfileDto();
        List<ProfileDto> profileDtos = new ArrayList<>();

        profileDto.setName("test_profile");
        profileDto.setMonth("APR");
        profileDto.setRatio(0.25);
        profileDtos.add(profileDto);
        profileDto.setMonth("MAY");
        profileDtos.add(profileDto);
        profileDto.setMonth("JUN");
        profileDtos.add(profileDto);

        profileMapper.updateDtoWithYear(profileDtos, year);

        profileDtos.forEach(dto -> assertTrue("Checking year: ", 2018 == dto.getYear()));
    }
}
