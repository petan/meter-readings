package com.jobinterview.meterreadings.mapper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ReadingDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReadingMapperImplTest {

    @Autowired
    ReadingMapper readingMapper;

    @Test
    public void mapToDto_success() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2018");
        ReadingIdentity readingIdentity = new ReadingIdentity(15L, date);
        Reading reading = new Reading(readingIdentity, "profile_C", 550L);

        ReadingDto readingDto = readingMapper.mapToDto(reading);

        assertTrue("Checking connetionID:", 15L == readingDto.getConnectionID());
        assertEquals("Checking month:", "Mar", readingDto.getMonth());
        assertTrue("Checking year:", 2018 == readingDto.getYear());
        assertEquals("Checking profile name:", "profile_C", readingDto.getProfileName());
        assertTrue("Checking reading:", 550L == readingDto.getReading());
    }

    @Test
    public void mapToDtoList_success() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("31/03/2018");
        ReadingIdentity readingIdentity = new ReadingIdentity(15L, date);
        Reading reading = new Reading(readingIdentity, "profile_C", 550L);

        List<Reading> readings = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            readings.add(reading);
        }
        List<ReadingDto> readingDtos = readingMapper.mapToDto(readings);

        assertTrue("Checking element count: ", 12 == readingDtos.size());
        readingDtos.forEach(readingDto -> {
            assertTrue("Checking connetionID:", 15L == readingDto.getConnectionID());
            assertEquals("Checking month:", "Mar", readingDto.getMonth());
            assertTrue("Checking year:", 2018 == readingDto.getYear());
            assertEquals("Checking profile name:", "profile_C", readingDto.getProfileName());
            assertTrue("Checking reading:", 550L == readingDto.getReading());
        });
    }

    @Test
    public void mapFromDto_withCurrentYear_success() throws Exception {

        ReadingDto readingDto = new ReadingDto();
        readingDto.setConnectionID(15L);
        readingDto.setMonth("APR");
        readingDto.setYear(2015);
        readingDto.setProfileName("profile_A");
        readingDto.setReading(5414L);

        Reading reading = readingMapper.mapFromDto(readingDto);
        Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("30/04/2015 23:59:59.999");

        assertTrue("Checking connectionID:", 15L == reading.getReadingIdentity().getConnectionID());
        assertEquals("Checking date:", date, reading.getReadingIdentity().getDate());
        assertEquals("Checking profile name:", "profile_A", reading.getProfileName());
        assertTrue("Checking reading:", 5414L == reading.getReading());
    }

    @Test
    public void mapFromDtoList_withoutCurrentYear_success() throws Exception {

        ReadingDto readingDto = new ReadingDto();
        readingDto.setConnectionID(15L);
        readingDto.setMonth("APR");
        readingDto.setProfileName("profile_A");
        readingDto.setReading(5414L);

        List<ReadingDto> readingDtos = new ArrayList<>();
        for (int i = 0; i < 12; i++) {
            readingDtos.add(readingDto);
        }
        List<Reading> readings = readingMapper.mapFromDto(readingDtos);

        Calendar c = Calendar.getInstance();
        int currentYear = c.get(Calendar.YEAR); //without year, system time year is assumed
        String stringDate = "30/04/" + currentYear + " 23:59:59.999";
        Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse(stringDate);

        assertTrue("Checking element count: ", 12 == readings.size());
        readings.forEach(r -> {
            assertTrue("Checking connectionID:", 15L == r.getReadingIdentity().getConnectionID());
            assertEquals("Checking date:", date, r.getReadingIdentity().getDate());
            assertEquals("Checking profile name:", "profile_A", r.getProfileName());
            assertTrue("Checking reading:", 5414L == r.getReading());
        });
    }
}
