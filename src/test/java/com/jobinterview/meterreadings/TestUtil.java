package com.jobinterview.meterreadings;

import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.util.DateUtil;
import org.springframework.http.MediaType;

public class TestUtil {

    public static final MediaType APPLICATION_JSON_UTF8 = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(object);
    }

    public static <T> byte[] convertListToJsonBytes(List<T> list) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        return mapper.writeValueAsBytes(list);
    }

    public static List<ProfileDto> generateProfileDtosForYear(String name, Integer year, int[] includedMonths, Double[] ratios) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<ProfileDto> profileDtos = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
            profileDtos.add(new ProfileDto(name, shortMonths[includedMonths[i]].toUpperCase(), year, ratios[i]));
        }

        return profileDtos;
    }

    public static List<Profile> generateProfilesForYear(String name, Integer year, int[] includedMonths, Double[] ratios) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<Profile> profiles = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
            Date date = DateUtil.dateFromYearAndMonth(year, shortMonths[includedMonths[i]]);
            profiles.add(new Profile(new ProfileIdentity(name, date), ratios[i]));
        }

        return profiles;
    }

    public static List<ReadingDto> generateReadingDtosForYear(Long connectionId, Integer year, int[] includedMonths, String profileName, Long[] measurements) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<ReadingDto> readingDtos = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
            readingDtos.add(new ReadingDto(connectionId, profileName, shortMonths[includedMonths[i]].toUpperCase(), year, measurements[i]));
        }

        return readingDtos;
    }

    public static List<Reading> generateReadingsForYear(Long connectionId, Integer year, int[] includedMonths, String profileName, Long[] measurements) {
        String[] shortMonths = new DateFormatSymbols(Locale.ENGLISH).getShortMonths();
        List<Reading> readings = new ArrayList<>();
        for (int i = 0; i < includedMonths.length; i++) {
            Date date = DateUtil.dateFromYearAndMonth(year, shortMonths[includedMonths[i]]);
            readings.add(new Reading(new ReadingIdentity(connectionId, date), profileName, measurements[i]));
        }

        return readings;
    }
}
