package com.jobinterview.meterreadings.util;

import static org.junit.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

public class DateUtilTest
{

    @Test
    public void dateFromYearAndMonth_validYearAndMonth_success() throws Exception {
        Integer year = 2017;
        String month = "SEP";

        Date result = DateUtil.dateFromYearAndMonth(year, month);
        Date expected = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("30/09/2017 23:59:59.999");

        assertEquals("Checking valid parsed date from month and year: ", expected, result);
    }

    @Test
    public void dateFromYearAndMonth_validMonthYearNull_success() throws Exception {
        Integer year = null;
        String month = "SEP";

        Date result = DateUtil.dateFromYearAndMonth(year, month);
        Calendar c = Calendar.getInstance();
        int expectedYear = c.get(Calendar.YEAR); //without year, system time year is assumed
        String expectedStringDate = "30/09/" + expectedYear + " 23:59:59.999";
        Date expected = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse(expectedStringDate);

        assertEquals("Checking valid parsed date from month and year: ", expected, result);
    }

    @Test
    public void dateFromYearAndMonth_ValidYearAndInvalidMonth_fail() throws Exception {
        Integer year = 2017;
        String month = "09";

        Date result = DateUtil.dateFromYearAndMonth(year, month);

        assertEquals("Checking valid parsed date from month and year: ", null, result);
    }

    @Test
    public void monthFromDateTest_success() throws Exception {
        Date date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("31/10/2015 23:59:59.999");

        assertEquals("Checking month from date: ", "Oct", DateUtil.monthFromDate(date, "MMM"));
    }
}
