package com.jobinterview.meterreadings.util;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Test;

public class NumberUtilTest {

    @Test
    public void isDouble_success() throws Exception {

        assertTrue("Checking valid double: ", NumberUtil.isDouble("5"));
        assertTrue("Checking valid double: ", NumberUtil.isDouble("451.322"));
    }

    @Test
    public void isDouble_fail() throws Exception {

        assertFalse("Checking invalid double: ", NumberUtil.isDouble("a"));
        assertFalse("Checking invalid double: ", NumberUtil.isDouble("12b3"));
    }
}
