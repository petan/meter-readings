package com.jobinterview.meterreadings.service;

import static org.mockito.Mockito.doReturn;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.domain.ReadingIdentity;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.repository.ReadingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReadingServiceImplTest {

    @Autowired
    ReadingService readingService;

    @MockBean
    ReadingRepository readingRepository;

    @Test
    public void getReadings_noParameters_findAll() throws Exception {

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Reading> readings = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);

        doReturn(readings).when(readingRepository).findReadings(new HashMap<>());

        List<ReadingDto> found = readingService.getReadings(new HashMap<>());

        Assert.assertEquals("checking equal: ", 12, found.size());
    }

    @Test
    public void getReadings_ParameterYearProvided_find() throws Exception {

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Reading> readings = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> additional = TestUtil.generateReadingsForYear(3L, 2017, includedMonths, "B", measurements);

        Map<String, String> searchParameters = new HashMap<>();
        searchParameters.put("year", "2017");
        doReturn(additional).when(readingRepository).findReadings(searchParameters);

        List<ReadingDto> found = readingService.getReadings(searchParameters);

        Assert.assertEquals("checking equal: ", 12, found.size());
    }

    @Test
    public void getReadingByReadingIdentity() throws Exception {

        Reading reading = new Reading(new ReadingIdentity(3L, new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("31/01/2018 23:59:59.999")), "C", 50L);

        doReturn(reading).when(readingRepository).findByReadingIdentity(reading.getReadingIdentity());

        Reading found = readingService.getReadingByReadingIdentity(reading.getReadingIdentity());

        Assert.assertEquals("checking equal: ", reading, found);
    }

    @Test
    public void deleteReadingsByNameAndYear_OnlyYearProvided_success() throws Exception {

        doReturn(12).when(readingRepository).deleteReadingsByConnectionAndYear(null, 2018);

        int deleted = readingService.deleteReadingsByConnectionAndYear(null, 2018);

        Assert.assertEquals("checking equal: ", 12, deleted);
    }
}
