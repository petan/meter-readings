package com.jobinterview.meterreadings.service;

import static org.mockito.Mockito.doReturn;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.ProfileIdentity;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.repository.ProfileRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileServiceImplTest {

    @Autowired
    ProfileService profileService;

    @MockBean
    ProfileRepository profileRepository;

    @Test
    public void getProfiles_noParameters_findAll() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> profiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);

        doReturn(profiles).when(profileRepository).findProfiles(new HashMap<>());

        List<ProfileDto> found = profileService.getProfiles(new HashMap<>());

        Assert.assertEquals("checking equal: ", 12, found.size());
    }

    @Test
    public void getProfiles_ParameterNameProvided_find() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> profiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        List<Profile> additional = TestUtil.generateProfilesForYear("B", 2018, includedMonths, ratios);
        profiles.addAll(additional);

        Map<String, String> searchParameters = new HashMap<>();
        searchParameters.put("name", "B");
        doReturn(additional).when(profileRepository).findProfiles(searchParameters);

        List<ProfileDto> found = profileService.getProfiles(searchParameters);

        Assert.assertEquals("checking equal: ", 12, found.size());
    }

    @Test
    public void getProfileByProfileIdentity() throws Exception {

        Profile profile = new Profile(new ProfileIdentity("A", new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS").parse("31/01/2018 23:59:59.999")), 0.1);

        doReturn(profile).when(profileRepository).findByProfileIdentity(profile.getProfileIdentity());

        Profile found = profileService.getProfileByProfileIdentity(profile.getProfileIdentity());

        Assert.assertEquals("checking equal: ", profile, found);
    }

    @Test
    public void deleteProfilesByNameAndYear() throws Exception {

        doReturn(12).when(profileRepository).deleteProfilesByNameAndYear("profileB", 2018);

        int deleted = profileService.deleteProfilesByNameAndYear("profileB", 2018);

        Assert.assertEquals("checking equal: ", 12, deleted);
    }
}
