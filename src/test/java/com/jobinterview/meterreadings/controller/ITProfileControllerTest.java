package com.jobinterview.meterreadings.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.dto.ProfileDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRun.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestRun.sql")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ITProfileControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getAllProfiles() throws Exception {

        mockMvc.perform(get("/api/profiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(36)))
                .andExpect(jsonPath("$[0].name", is("A")))
                .andExpect(jsonPath("$[0].month", is("Jan")))
                .andExpect(jsonPath("$[0].year", is(2017)))
                .andExpect(jsonPath("$[0].ratio", is(0.1)))
                .andExpect(jsonPath("$[1].name", is("A")))
                .andExpect(jsonPath("$[1].month", is("Feb")))
                .andExpect(jsonPath("$[1].year", is(2017)))
                .andExpect(jsonPath("$[1].ratio", is(0.1)));
    }

    @Test
    public void getAllProfiles_WithParameters() throws Exception {

        mockMvc.perform(get("/api/profiles?name=B")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].name", is("B")))
                .andExpect(jsonPath("$[0].month", is("Jan")))
                .andExpect(jsonPath("$[1].year", is(2018)))
                .andExpect(jsonPath("$[0].ratio", is(0.05)))
                .andExpect(jsonPath("$[1].name", is("B")))
                .andExpect(jsonPath("$[1].month", is("Feb")))
                .andExpect(jsonPath("$[1].year", is(2018)))
                .andExpect(jsonPath("$[1].ratio", is(0.1)));
    }

    @Test
    public void deleteProfiles_WithYearAndName_success() throws Exception {

        mockMvc.perform(delete("/api/profiles?year=2017&name=A")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(content().string(is("12")));
    }

    @Test
    public void deleteProfiles_WithYearOnly_success() throws Exception {

        mockMvc.perform(delete("/api/profiles?year=2018")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(content().string(is("24")));
    }

    @Test
    public void deleteProfiles_WithoutParameters_fail() throws Exception {

        mockMvc.perform(delete("/api/profiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteProfiles_NotFound() throws Exception {

        mockMvc.perform(delete("/api/profiles?year=2986")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void saveProfiles_success() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileDto> profileDtos = TestUtil.generateProfileDtosForYear("test_create", null, includedMonths, ratios);

        mockMvc.perform(post("/api/profiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertListToJsonBytes(profileDtos))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")))
                .andExpect(jsonPath("$.success[0].name", is("test_create")));
    }


    @Test
    public void updateProfiles_success() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileDto> profileDtos = TestUtil.generateProfileDtosForYear("test_update", null, includedMonths, ratios);

        mockMvc.perform(put("/api/profiles/year/2016")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertListToJsonBytes(profileDtos))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")))
                .andExpect(jsonPath("$.success[0].name", is("test_update")));
    }
}
