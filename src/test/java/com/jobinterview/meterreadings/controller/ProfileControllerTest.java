package com.jobinterview.meterreadings.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ProfileSaveUpdateDto;
import com.jobinterview.meterreadings.mapper.ProfileMapper;
import com.jobinterview.meterreadings.service.ProfileService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ProfileControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ProfileService profileServiceMock;

    @Test
    public void getAllProfiles_noParameters_resultsFound() throws Exception {

        ProfileMapper profileMapperMock = mock(ProfileMapper.class);

        List<ProfileDto> mockProfiles = new ArrayList<>();
        mockProfiles.add(new ProfileDto("A", "Jan", 2018, 0.1));
        mockProfiles.add(new ProfileDto("A", "Feb", 2018, 0.3));
        mockProfiles.add(new ProfileDto("A", "Mar", 2018, 0.5));

        List<ProfileDto> profileDtos = new ArrayList<>();
        profileDtos.add(new ProfileDto("A", "Jan", 2018, 0.1));
        profileDtos.add(new ProfileDto("A", "Feb", 2018, 0.3));
        profileDtos.add(new ProfileDto("A", "Mar", 2018, 0.5));

        when(profileServiceMock.getProfiles(any())).thenReturn(mockProfiles);
        when(profileMapperMock.mapToDto(anyList())).thenReturn(profileDtos);

        mockMvc.perform(get("/api/profiles"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void saveProfileRatios_correctData() throws Exception {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileDto> mockProfileDtos = TestUtil.generateProfileDtosForYear("A", 2018, includedMonths, ratios);
        ProfileSaveUpdateDto dto = new ProfileSaveUpdateDto();
        dto.getSuccess().addAll(mockProfileDtos);

        when(profileServiceMock.saveAllProfileDtos(any())).thenReturn(dto);

        mockMvc.perform(post("/api/profiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(mockProfileDtos))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")));
    }

    @Test
    public void deleteProfilesByYear_resultsFoundAndDeleted() throws Exception {

        int expectedDeletedCount = 12;

        when(profileServiceMock.deleteProfilesByNameAndYear(anyString(), anyInt())).thenReturn(expectedDeletedCount);

        mockMvc.perform(delete("/api/profiles?year=2015&name=B"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("12")));
    }

    @Test
    public void deleteProfilesByYear_resultsNotFoundNothingToDelete() throws Exception {

        int expectedDeletedCount = 0;

        when(profileServiceMock.deleteProfilesByNameAndYear(anyString(), anyInt())).thenReturn(expectedDeletedCount);

        mockMvc.perform(delete("/api/profiles?year=2015&name=B"))
                .andExpect(status().isNotFound());
    }
}
