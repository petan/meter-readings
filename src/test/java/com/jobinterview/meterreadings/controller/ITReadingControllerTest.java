package com.jobinterview.meterreadings.controller;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.dto.ProfileDto;
import com.jobinterview.meterreadings.dto.ReadingDto;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@Sql(executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:beforeTestRun.sql")
@Sql(executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD, scripts = "classpath:afterTestRun.sql")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ITReadingControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getAllReadings() throws Exception {

        mockMvc.perform(get("/api/readings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(36)))
                .andExpect(jsonPath("$[0].connectionID", is(1)))
                .andExpect(jsonPath("$[0].month", is("Jan")))
                .andExpect(jsonPath("$[0].year", is(2018)))
                .andExpect(jsonPath("$[0].profileName", is("A")))
                .andExpect(jsonPath("$[0].reading", is(10)))
                .andExpect(jsonPath("$[1].connectionID", is(1)))
                .andExpect(jsonPath("$[1].month", is("Feb")))
                .andExpect(jsonPath("$[1].year", is(2018)))
                .andExpect(jsonPath("$[1].profileName", is("A")))
                .andExpect(jsonPath("$[1].reading", is(20)));
    }

    @Test
    public void getAllReadings_WithParameters() throws Exception {

        mockMvc.perform(get("/api/readings?connectionId=2")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(12)))
                .andExpect(jsonPath("$[0].connectionID", is(2)))
                .andExpect(jsonPath("$[0].month", is("Jan")))
                .andExpect(jsonPath("$[0].year", is(2018)))
                .andExpect(jsonPath("$[0].profileName", is("B")))
                .andExpect(jsonPath("$[0].reading", is(500)))
                .andExpect(jsonPath("$[1].connectionID", is(2)))
                .andExpect(jsonPath("$[1].month", is("Feb")))
                .andExpect(jsonPath("$[1].year", is(2018)))
                .andExpect(jsonPath("$[1].profileName", is("B")))
                .andExpect(jsonPath("$[1].reading", is(1500)));
    }

    @Test
    public void deleteReadings_WithYearAndConnectionId_success() throws Exception {

        mockMvc.perform(delete("/api/readings?year=2018&connectionId=1")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(content().string(is("12")));
    }

    @Test
    public void deleteReadings_WithYearOnly_success() throws Exception {

        mockMvc.perform(delete("/api/readings?year=2018")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(content().string(is("24")));
    }

    @Test
    public void deleteReadings_WithoutParameters_fail() throws Exception {

        mockMvc.perform(delete("/api/readings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteReadings_NotFound() throws Exception {

        mockMvc.perform(delete("/api/readings?year=2986")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
        )
                .andExpect(status().isNotFound());
    }

    @Test
    public void saveReadings_success() throws Exception {

        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ProfileDto> profileDtos = TestUtil.generateProfileDtosForYear("test_create", null, includedMonths, ratios);

        mockMvc.perform(post("/api/profiles")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertListToJsonBytes(profileDtos))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")))
                .andExpect(jsonPath("$.success[0].name", is("test_create")));

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        List<ReadingDto> readingDtos = TestUtil.generateReadingDtosForYear(15L, null, includedMonths, "test_create", measurements);

        mockMvc.perform(post("/api/readings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertListToJsonBytes(readingDtos))
        )
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")))
                .andExpect(jsonPath("$.success[0].connectionID", is(15)))
                .andExpect(jsonPath("$.success[0].profileName", is("test_create")));
    }
}
