package com.jobinterview.meterreadings.controller;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.ArrayList;
import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.dto.ReadingDto;
import com.jobinterview.meterreadings.dto.ReadingSaveUpdateDto;
import com.jobinterview.meterreadings.mapper.ReadingMapper;
import com.jobinterview.meterreadings.service.ReadingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class ReadingControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ReadingService readingServiceMock;

    @Test
    public void getAllReadings_noParameters_resultsFound() throws Exception {

        ReadingMapper readingMapperMock = mock(ReadingMapper.class);

        List<ReadingDto> mockReadings = new ArrayList<>();
        mockReadings.add(new ReadingDto(3L, "C", "JAN", 2018, 50L));
        mockReadings.add(new ReadingDto(3L, "C", "FEB", 2018, 150L));
        mockReadings.add(new ReadingDto(3L, "C", "MAR", 2018, 220L));

        List<ReadingDto> readingDtos = new ArrayList<>();
        readingDtos.add(new ReadingDto(3L, "C", "JAN", 2018, 50L));
        readingDtos.add(new ReadingDto(3L, "C", "FEB", 2018, 150L));
        readingDtos.add(new ReadingDto(3L, "C", "MAR", 2018, 220L));

        when(readingServiceMock.getReadings(any())).thenReturn(mockReadings);
        when(readingMapperMock.mapToDto(anyList())).thenReturn(readingDtos);

        mockMvc.perform(get("/api/readings"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(TestUtil.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    public void saveReadingRatios_correctData() throws Exception {

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<ReadingDto> mockReadingDtos = TestUtil.generateReadingDtosForYear(3L, 2018, includedMonths, "A", measurements);
        ReadingSaveUpdateDto dto = new ReadingSaveUpdateDto();
        dto.getSuccess().addAll(mockReadingDtos);

        when(readingServiceMock.saveAllReadingDtos(anyList())).thenReturn(dto);

        mockMvc.perform(post("/api/readings")
                .contentType(TestUtil.APPLICATION_JSON_UTF8)
                .content(TestUtil.convertObjectToJsonBytes(mockReadingDtos))
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.success", hasSize(12)))
                .andExpect(jsonPath("$.fail", hasSize(0)))
                .andExpect(jsonPath("$.status.errorMessage", is("OK")));
    }

    @Test
    public void deleteReadingsByYear_resultsFoundAndDeleted() throws Exception {

        int expectedDeletedCount = 12;

        when(readingServiceMock.deleteReadingsByConnectionAndYear(anyLong(), anyInt())).thenReturn(expectedDeletedCount);

        mockMvc.perform(delete("/api/readings?year=2015&connectionId=3"))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo("12")));
    }

    @Test
    public void deleteReadingsByYear_resultsNotFoundNothingToDelete() throws Exception {

        int expectedDeletedCount = 0;

        when(readingServiceMock.deleteReadingsByConnectionAndYear(anyLong(), anyInt())).thenReturn(expectedDeletedCount);

        mockMvc.perform(delete("/api/readings?year=2015&connectionId=265"))
                .andExpect(status().isNotFound());
    }
}
