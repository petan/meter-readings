package com.jobinterview.meterreadings.validator;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.domain.Profile;
import com.jobinterview.meterreadings.domain.Reading;
import com.jobinterview.meterreadings.service.ProfileService;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ReadingValidatorTest {

    @Autowired
    ReadingValidator readingValidator;

    @MockBean
    ProfileService profileService;

    @Test
    @Ignore
    public void test_validation_allValid() {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> sortedProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        List<Reading> validReadings = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> expectedValidList = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        doReturn(sortedProfiles).when(profileService).getSortedListOfRelevantProfiles(validReadings, 2018);
        readingValidator.validate(validReadings, failList, errors);

        Assert.assertEquals("checking valid reading list:", expectedValidList, validReadings);
        Assert.assertEquals("checking fail list: ", 0, failList.size());
        Assert.assertEquals("checking errors count: ", 0, errors.size());
    }

    @Test
    public void test_validation_failConsumption() {
        ProfileService profileService = mock(ProfileService.class);
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> sortedProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);

        Long[] measurements = {100L, 105L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        List<Reading> validReadings = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> expectedFailList = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        doReturn(sortedProfiles).when(profileService).getSortedListOfRelevantProfiles(validReadings, 2018);
        readingValidator.validate(validReadings, failList, errors);

        Assert.assertEquals("checking valid reading list:", 0, validReadings.size());
        Assert.assertEquals("checking fail list: ", expectedFailList, failList);
        Assert.assertEquals("checking errors count: ", 1, errors.size());
    }

    @Test
    public void test_validation_DateEmptyThree() {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> sortedProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);

        Long[] measurements = {100L, 200L, 300L, 400L, 450L, 500L, 600L, 700L, 800L, 850L, 900L, 1000L};
        List<Reading> validReadings = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> expectedValidList = TestUtil.generateReadingsForYear(3L, 2018, includedMonths, "A", measurements);
        List<Reading> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        validReadings.get(3).getReadingIdentity().setDate(null);
        validReadings.get(7).getReadingIdentity().setDate(null);
        validReadings.get(11).getReadingIdentity().setDate(null);

        doReturn(sortedProfiles).when(profileService).getSortedListOfRelevantProfiles(validReadings, 2018);
        readingValidator.validate(validReadings, failList, errors);

        Assert.assertEquals("checking valid reading list:", 0, validReadings.size());
        Assert.assertEquals("checking fail list: ", 12, failList.size());
    }
}
