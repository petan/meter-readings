package com.jobinterview.meterreadings.validator;

import java.util.ArrayList;
import java.util.List;

import com.jobinterview.meterreadings.TestUtil;
import com.jobinterview.meterreadings.domain.Profile;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProfileValidatorTest {

    @Autowired
    ProfileValidator profileValidator;


    @Test
    public void test_validation_allValid() {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> validProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        List<Profile> expectedValidList = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        List<Profile> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        profileValidator.validate(validProfiles, failList, errors);

        Assert.assertEquals("checking valid profile list:", expectedValidList, validProfiles);
        Assert.assertEquals("checking fail list: ", 0, failList.size());
        Assert.assertEquals("checking errors count: ", 0, errors.size());

    }

    @Test
    public void test_validation_failRatioSum() {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.5, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> validProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        List<Profile> expectedFailList = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        List<Profile> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        profileValidator.validate(validProfiles, failList, errors);

        Assert.assertEquals("checking valid profile list:", 0, validProfiles.size());
        Assert.assertEquals("checking fail list: ", expectedFailList, failList);
        Assert.assertEquals("checking errors count: ", 1, errors.size());

    }

    @Test
    public void test_validation_NameEmptyThree() {
        Double[] ratios = {0.1, 0.1, 0.1, 0.1, 0.05, 0.05, 0.5, 0.1, 0.1, 0.05, 0.05, 0.1};
        int[] includedMonths = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
        List<Profile> validProfiles = TestUtil.generateProfilesForYear("A", 2018, includedMonths, ratios);
        validProfiles.get(3).getProfileIdentity().setName("");
        validProfiles.get(5).getProfileIdentity().setName("");
        validProfiles.get(8).getProfileIdentity().setName("");

        List<Profile> failList = new ArrayList<>();
        List<String> errors = new ArrayList<>();

        profileValidator.validate(validProfiles, failList, errors);

        Assert.assertEquals("checking valid profile list:", 0, validProfiles.size());
        Assert.assertEquals("checking fail list: ", 12, failList.size());

    }
}
